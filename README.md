Motivational presentation about learning to code using a Raspberry Pi.

The how's

- What could you expect from a home computer in the 80?
- How did you program it?
- Was is fun? (spoiler, yes it was).

The why's

- Why the Raspberry Pi was built
- Why should you use it to learn programming.
